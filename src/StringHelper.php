<?php
/**
 * User: wolfDiesel
 * Email: wolf.diesel.work@gmail.com
 */

namespace WDieselHelpers\Helpers;

class StringHelper
{
    public static function getRandom($max_length = 10, $with_nums = false)
    {
        $alpha = 'qwertyuiopasdfghjklzxcvbnm';
        $nums = '1234567890';
        $ret = '';
        if ($with_nums == true) {
            $alpha .= $nums;
        }
        for ($i = 0; $i < $max_length; $i++) {
            $ret .= $alpha[rand(0, (strlen($alpha) - 1))];
        }

        return $ret;
    }
}